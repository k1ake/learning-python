# learning-python

## Description
Some programms that i wrote while learning python. Every programm has a little readme with dependencies, how to build them and small description what they do
Some stuff could has bugs or defects, most of them also will be listed in readme. I'll appreciate any advices or pull requests with fixes or new idea for that projects
Everything licensed under GPL so feel free to use my code

## Roadmap

- [x] Python basic syntax
- [x] `While`, `for` cycles
- [x] `Match | case` statements
- [x] Importing libraries
- [x] `os` and `subprocess` modules
- [x] Writing own functions
- [x] JSON and how to work with files
- [x] .ini config files
- [x] argparsing
- [x] API requests
- [x] GTK basics
- [x] Telegram Bots
- [ ] Django
- [ ] SQL
- [ ] Sockets

***

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
Feel free to request features or make pull requests

## Project status
Still learning python!
