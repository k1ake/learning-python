import os
import random
import time

clear = lambda: os.system('clear')
curpath = lambda: os.system('pwd')


# Игрок:
# Имеет хп, оружие, зелья хп
# Противники: имеют хп, урон
# 
# Бой:
# 4 действия у игрока:
# уклониться влево
# уклониться назад
# уклониться вправо
# попытаться атаковать
# При успешном уклонении предлагается 100% успешная атака
# или выпить зелье
# Попытка атаковать наносит урон с некоторым шансом
# 
# Противник атакует каждый ход с одной из сторон,
# может промахнуться если игрок успешно уклонился
# 
# Оружие:
# Имеет характеристики: урон, шанс крита, множитель крита
# При нахождении оружия у игрока спрашивают поменять ли текущее
# 
# Обычное оружие с шансом в может выпасть с противника
# Шанс крита - 0%
# Хорошее оружие с шансом может выпасть с противника
# Шанс крита - 10-20% Множитель 1.3
# Отличное оружие получается из скоровищ или боссов
# Шанс крита 30-50% множитель 1.5-2
# 
# Зелье здоровья:
# Восстанавливает 100 здоровья
# С шансом в падает с противников
# С шансом в находится в сокровищах


#  ____        _   _   _
# | __ )  __ _| |_| |_| | ___
# |  _ \ / _` | __| __| |/ _ \
# | |_) | (_| | |_| |_| |  __/
# |____/ \__,_|\__|\__|_|\___|

def battle():
    global lvl,sword,pots,hp,beaten, en_hp, en_at
    enemy_spawn()
    dodge=0
    stuck=["You've упал","You haven't decided what to do"]
    print("Your Hp: ",hp,'/',max_hp,sep='')
    print("Potions in backpack: ", pots)
    print("Your weapon is:",sword[-1])
    input('Press Enter to let the battle begin')
    battle=True
    clear()
    while battle:
        chp=en_hp+random.randint(-2,2)
        if chp<=0:
            chp=1
        print("Your hp is: ",hp,", Pots: ",pots, sep='')
        print("Enemie's hp is around",chp)
        print('Enemy swinging its weapon\n(1) - dodge left (2) - step back\n(3) - dodge right (4) - try to attack',sep='')
        match input():
            case "1":
                print("You've tryed to dodge")
                dodge=1
            case "2":
                print("You've tryed to dodge")
                dodge=2
            case "3":
                print("You've tryed to dodge")
                dodge=3
            case "4":
                if random.randint(1,2)==2:
                    print("You've hurt it")
                    en_hp-=punch()
                    if en_hp<=0:
                        print("You've killed it")
                        break
                else: print("You've missed")
                dodge=0
            case _:
                print(stuck[random.randint(0,len(stuck)-1)])
                dodge=0
        if dodge!=0:
            if random.randint(0,9)>4:
                print("You've dodged an attack!")
                print("What will you do?\n(1) Strike the enemy (2)Drink potion[",pots,"]",sep='')
                match input():
                    case "1":
                        en_hp-=punch()
                        print("You've hurt it")
                        if en_hp<=0:
                            print("You've killed it")
                            break
                    case "2":
                        if pots>0:
                            hp+=50
                            pots-=1
                            if hp>max_hp:
                                hp=max_hp
                            print("You've drunk HP potion, your hp now is ",hp,"/",max_hp,sep='')
                        else:
                            print("You've tryed to drink a potion, but nothing is in the bottle")
                    case _:
                        print("You've decided to do nothing")
            else:
                hp-=en_at
                if hp>0:
                    print("Enemy got you, your hp now is:",hp)
        else:
            hp-=en_at
            if hp>0:
                print("You've recieved a strike, your hp is:",hp)
        dodge=0
        input("Press Enter to continue..")
        clear()
    if hp<=0:
        print("You died")
        return 0
    else:
        beaten+=1
        input("Press Enter to move to the next room")
        if random.randint(0,100)>=40:
            clear()
            print("You've found new weapon!")
            new_sword=weapon_get()
            print("It's",new_sword[3])
            print("Damage =",new_sword[0],"Crit Chance =",new_sword[1],"%")
            print("-------------------------------------")
            print("Current weapon is:",sword[3])
            print("Damage =",sword[0],"Crit Chance =",sword[1])
            match input("Would you use it? (y/n)"):
                case "y":
                    sword=new_sword
                    print("You've decided to use it")
                case "n":
                    print("You've decided not to use it")
                case _:
                    print("That's a hard choice for you, so you decided to move on")
        if random.randint(0,100)>=70:
            print("You found a HP potion")
            pots+=1
    input("Press Enter to continue...")

#  _____ ____  _____    _    ____  _   _ ____  _____
# |_   _|  _ \| ____|  / \  / ___|| | | |  _ \| ____|
#   | | | |_) |  _|   / _ \ \___ \| | | | |_) |  _|
#   | | |  _ <| |___ / ___ \ ___) | |_| |  _ <| |___
#   |_| |_| \_\_____/_/   \_\____/ \___/|_| \_\_____|
 
def treasure(a=0):
    global lvl, sword, new_sword, pots
    clear()
    new_sword=weapon_get(a)
    new_pots=random.randint(0,a)
    print("You found a treasure room!")
    input("Press Enter to explore it")
    clear()
    print("You've found new weapon and",new_pots,"potion(s)!!")
    print("It's",new_sword[3])
    print("Damage =",new_sword[0],"Crit Chance =",new_sword[1],"%")
    print("-------------------------------------")
    print("Current weapon is:",sword[3])
    print("Damage =",sword[0],"Crit Chance =",sword[1])
    match input("Would you use it? (y/n)"):
        case "y":
            sword=new_sword
            print("You've decided to use it")
        case "n":
            print("You've decided not to use it")
        case _:
            print("That's a hard choice for you, so you decided to move on")
        
                             
#   ___                         _                   _
#  / _ \ _ __   ___ _ __  _ __ (_)_ __   __ _    __| | ___   ___  _ __ ___
# | | | | '_ \ / _ \ '_ \| '_ \| | '_ \ / _` |  / _` |/ _ \ / _ \| '__/ __|
# | |_| | |_) |  __/ | | | | | | | | | | (_| | | (_| | (_) | (_) | |  \__ \
#  \___/| .__/ \___|_| |_|_| |_|_|_| |_|\__, |  \__,_|\___/ \___/|_|  |___/
#       |_|                             |___/

def moving():
    global doors, door, path, blackpath, lvl, pots, hp
    check=True
    while check:
        print('There are ',len(doors),' doors, which one will you choose? (1-',len(doors),')',sep='')
        nextd=input(": ")
        if nextd.isdigit() and int(nextd)<=len(doors) and int(nextd)>0:
            trypath=path+doors[int(nextd)-1]+'/'
            if trypath in blackpath:
                print("That door is closed")
                doors=doors[0:int(nextd)-1]+doors[int(nextd):len(doors)]
            else:
                try: os.listdir(trypath)
                except PermissionError:
                    print("That door is for sudo users only")
                    doors=doors[0:int(nextd)-1]+doors[int(nextd):len(doors)]
                else:
                    door=doors[int(nextd)-1]
                    path=path+door+'/'
                    check=False
                    lvl+=1
                    input("Press Enter to continue..")
                    clear()
        else:
            print('There is no such door')
            input("Press Enter to rechoose")
            clear()


#  _____                               ____                _   _
# | ____|_ __   ___ _ __ ___  _   _   / ___|_ __ ___  __ _| |_(_) ___  _ __
# |  _| | '_ \ / _ \ '_ ` _ \| | | | | |   | '__/ _ \/ _` | __| |/ _ \| '_ \
# | |___| | | |  __/ | | | | | |_| | | |___| | |  __/ (_| | |_| | (_) | | | |
# |_____|_| |_|\___|_| |_| |_|\__, |  \____|_|  \___|\__,_|\__|_|\___/|_| |_|
#                             |___/
                            
def enemy_spawn():
    global lvl, en_hp, en_at
    if lvl%5==0:
        en_hp=int(10*(lvl+2.5)+random.randint(0,5))
        en_at=int(5+lvl*random.randint(110,130)*0.01)
        print("Boss appeared!")
    elif lvl%3==0:
        en_hp=int(10*(lvl+1.5)+random.randint(-3,3))
        en_at=int(3+lvl)
        print("Giant enemy appeared!")
    else:
        en_hp=10*(lvl+1)+random.randint(-2,2)
        en_at=int(2+lvl)
        print("Normal enemy appeared!")
    


#  _____               _              __        __
# |  ___|__  _ __ __ _(_)_ __   __ _  \ \      / /__  __ _ _ __   ___  _ __  ___
# | |_ / _ \| '__/ _` | | '_ \ / _` |  \ \ /\ / / _ \/ _` | '_ \ / _ \| '_ \/ __|
# |  _| (_) | | | (_| | | | | | (_| |   \ V  V /  __/ (_| | |_) | (_) | | | \__ \
# |_|  \___/|_|  \__, |_|_| |_|\__, |    \_/\_/ \___|\__,_| .__/ \___/|_| |_|___/
#                |___/         |___/                      |_|

def weapon_get(a=0):
    global lvl
    weapon_types=["Sword","Axe","Katana","Knife","Hummer"]
    weapon_prefix=["Iron ","Wooden ","Copper"]
    cool_weapon_prefix=["Big ","Shiny ","Awesome "]
    legendary_weapon_prefix=["Giant ","Legendary ","The Coolest "]

    if lvl%5==0 or a==2:
        damage=random.randint((lvl+1)*5,(lvl+3)*5)
        crit_chance=random.randint(30,50)
        crit_damage=1.5+0.01*random.randint(0,50)
        name=random.choice(legendary_weapon_prefix)+random.choice(weapon_types)
    elif lvl%3==0 or a==1:
        damage=random.randint((lvl+1)*5,(lvl+2)*5)
        crit_chance=random.randint(10,25)
        crit_damage=1+0.01*random.randint(0,30)
        name=random.choice(cool_weapon_prefix)+random.choice(weapon_types)
    else:
        damage=random.randint((lvl)*5,(lvl+1)*5)
        crit_chance=random.randint(0,10)
        crit_damage=1+0.01*random.randint(0,30)
        name=random.choice(weapon_prefix)+random.choice(weapon_types)

    
    new_sword=[damage,crit_chance,crit_damage,name]
    return new_sword


#  ____  _____    _    _       ____  __  __  ____
# |  _ \| ____|  / \  | |     |  _ \|  \/  |/ ___|
# | | | |  _|   / _ \ | |     | | | | |\/| | |  _
# | |_| | |___ / ___ \| |___  | |_| | |  | | |_| |
# |____/|_____/_/   \_\_____| |____/|_|  |_|\____|

def punch():
    global sword
    if random.randint(0,100)<=sword[1]:
        dmg=int(sword[0]*sword[2])
        print("Crit!")
    else:
        dmg=sword[0]
    return dmg

    
#  ____    _    ____  _____   ____ _____  _  _____ ____
# | __ )  / \  / ___|| ____| / ___|_   _|/ \|_   _/ ___|
# |  _ \ / _ \ \___ \|  _|   \___ \ | | / _ \ | | \___ \
# | |_) / ___ \ ___) | |___   ___) || |/ ___ \| |  ___) |
# |____/_/   \_\____/|_____| |____/ |_/_/   \_\_| |____/

name=''
win=False
path='/'
lvl=1
player_lvl=1
max_hp=100+(player_lvl-1)*50
hp=100
pots=1
beaten=0
blackpath=["/bin/","/lib/","/lib64/","/mnt/","/personal/","/sbin/","/srv/"]
# Weapon stats: DMG, Crit chance, crit multiplier, name
sword=[8,5,0,'Wooden Sword']

#  ___ _   _ _____ ____   ___
# |_ _| \ | |_   _|  _ \ / _ \
#  | ||  \| | | | | |_) | | | |
#  | || |\  | | | |  _ <| |_| |
# |___|_| \_| |_| |_| \_\\___/
def intro():
    global name
    print("Welcome traveler!!\n")
    name=input("What's your name?: ")
    clear()
    time.sleep(0.5)
    print(name,", you woke up in a dungeon, you need to get out of it as soon as possible",sep='')
    time.sleep(3)
    print("Next to you was a backpack with a wooden sword and 1 HP potion")
    time.sleep(3)
    print("Only thing you know is that you have to move towards")
    time.sleep(3)
    print("Good luck, ",name,'!!',sep='')
    time.sleep(1)
    input("Press Enter to start your journey")


#  _____ _   _ _____    ____    _    __  __ _____
# |_   _| | | | ____|  / ___|  / \  |  \/  | ____|
#   | | | |_| |  _|   | |  _  / _ \ | |\/| |  _|
#   | | |  _  | |___  | |_| |/ ___ \| |  | | |___
#   |_| |_| |_|_____|  \____/_/   \_\_|  |_|_____|

intro()
while not win:
    doors=[]
    weapon_get()
    for file in os.listdir(path):
        door=path+file+'/'
        # if os.path.isdir(door) and door not in blackpath:
        if os.path.isdir(door):
            doors.append(file)
    if doors==[] and lvl>=4:
        clear()
        print("Congratulations!!",name," you sucsessfily escaprd the dungeon\n")
        print("Stats:\n Doors opened: ",lvl-1,"\nBeaten enemies: ",beaten)
        print("Weapon is:",sword[-1],"\nDamage:",sword[1],"Crit Chance",sword[2],"Crit Multiplyer:",sword[-2])
        win == True
        break
    elif doors==[] and lvl<4:
        path="/home/"
    else:
        clear()
        if lvl%5==0:
            player_lvl+=1
            max_hp=100+(player_lvl-1)*50
            hp+=50
            if hp>max_hp:
                hp=max_hp
            print("Wow!",name," you are on the next level!\nHP pool is bigger now\nCurrent HP is: ",hp,"/",max_hp,sep='')
        moving()
    if random.randint(0,9)>=1:
        battle()
    elif random.randint(0,9)<1:
        if lvl%5==0:
            treasure(2)
        elif lvl%3==0:
            treasure(1)
        else: treasure()
        

