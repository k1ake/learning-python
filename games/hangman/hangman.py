import random, os
clear = lambda: os.system('clear')
easy_massive="ладонь, пылесос, король, зеркало, табурет, красота, переход, аквапарк, желание, конфета, песок, куртка, сказка, пирог, звезда, сахар, берег, горка, пирамида, рыба, телевизор, секрет, тесто, лужа, невидимка, кладовка, мультик, экран, магазин, занавеска".split(',')
normal_massive="полдник, учитель, снежинка, корабль, пряник, дружба, лимон, музыка, трещина, индульгент, свинья, опушка, силач, покрывало, амфитеатр, ресница, синоптик, фундамент, комбинация, кашпо, вращение, кордебалет, кондитер, пламя, галлюциноген, перпендикуляр, скоросшиватель, рефрижератор, иероглиф, подъезд".split(',')
hard_massive="эксгумация, либерализм, экспонат, пышность, скабрёзность, шаловливость, экспозиция, индульгенция, контрацептив, шкворень, эпиграф, эпитафия, барбекю, жульен, энцефалопатия, парашютист, импозантность, индифферент, демультипликатор, педикулёз, выхухоль, россомаха, сущность, поэтапность, напыщенность, возвышенность, контрстрельба, взбзднуть, галстук, флюгер".split(',')
def open_letters():
    global letter, guess, word
    for i in range(len(word)):
        if word[i] == letter.lower():
            guess[i] = letter.upper()
win=0
tries=0
tried=[]
print("Выберите тип игры:\n(1) - лёгкая (2) - нормальная\n(3) - сложная (4) - своё слово")
match input(':'):
    case '1': word=random.choice(easy_massive)
    case '2': word=random.choice(easy_massive)
    case '3': word=random.choice(easy_massive)
    case '4': word=input("Ваше слово:").lower()
clear()
guess=["_" for i in range(len(word))]
while True and win==0:
    print(f"{' '.join(guess)}\nНеправильных: {tries}/10")
    print(f"Уже использованные буквы: {'/'.join(tried)}")
    letter=input("Ваша буква?").lower()
    clear()
    if len(letter) > 1:
        print("Введите только 1 букву")
    elif letter not in word and letter not in tried:
        print("Нет такой буквы")
        tries+=1
        tried.append(letter.lower())
    elif letter in tried: 
        print("Уже есть")
    else: 
        open_letters()
        tried.append(letter.lower())
    if '_' not in guess and tries <10:
        win=2
        print(f"Слово: {word.upper()}\nТы победил!")
        input()
    elif tries == 10:
        win=1
        print(f"Ты проиграл!\nЗагаданное слово: {word.upper()}")
        input()
