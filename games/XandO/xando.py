import os
clear = lambda: os.system('clear')
clear()
count=0
g=False # player number
check=False
G=['один','два']
X=['X','O']
win=False #
p=[['-','-','-'], ['-','-','-'], ['-','-','-']]
print('',*p[0],'\n',*p[1],'\n',*p[2],'',sep='|')
while win!=True:
    a=0
    b=0
    print("Ход игрока номер",G[int(g)])
    while a==0:
        a=int(input())
        if a<1 or a>9:
            a=0
            print('Введите номер клетки (от 1-9)')
    while a>=4:
        a-=3
        b+=1
    if p[b][a-1]=='-':
        p[b][a-1]=X[int(g)]
        g=not g
        clear()
        count+=1
    else: 
        clear()
        print('Так ходить нельзя!')
    print('',*p[0],'\n',*p[1],'\n',*p[2],'',sep='|')
    if p[0][0]==p[0][1]==p[0][2] and p[0][0]!='-':
        win=True
    elif p[1][0]==p[1][1]==p[1][2] and p[1][0]!='-':
        win=True
    elif p[2][0]==p[2][1]==p[2][2] and p[2][0]!='-':
        win=True
    elif p[0][0]==p[1][0]==p[2][0] and p[0][0]!='-':
        win=True
    elif p[0][1]==p[1][1]==p[2][1] and p[0][1]!='-':
        win=True
    elif p[0][2]==p[1][2]==p[2][2] and p[0][2]!='-':
        win=True 
    elif p[0][0]==p[1][1]==p[2][2] and p[0][0]!='-':
        win=True
    elif p[0][2]==p[1][1]==p[2][0] and p[2][0]!='-':
        win=True
    if win==True:
        print('Победил игрок номер ', G[int(not g)],'(',X[int(not g)],')',sep='')
        break
    elif count==9:
        print('Ничья!')
        break
