import requests,sys, os
from datetime import datetime

from variables import appid, load_data, save_data, icons, wind_deg, city_check

url="https://api.openweathermap.org/data/2.5/forecast"
clear=lambda: os.system("clear")
clear()
def forecast():
    global r
    print("Input city")
    match input(": ").lower():
        case "load": r=load_data('dataforecast.json')
        case _ as city: 
            city_check(city)
            units="metric"
            params={"q":city, "cnt":40, "units":units, "appid":appid}
            r = requests.get(url, params=params).json()
            save_data(r,'dataforecast.json')
    return r

def w_dict(r):
    weather_dict=[]
    for i in range(len(r['list'])):
        new={'time':datetime.utcfromtimestamp(r['list'][i]['dt']).strftime("%H:%M | %d/%b"),
        'temp': r['list'][i]['main']['temp'],
        'pressure': r['list'][i]['main']['pressure'],
        'humidity': r['list'][i]['main']['humidity'],
        'weather': r['list'][i]['weather'][0]['main'].lower(),
        'description': r['list'][i]['weather'][0]['description'].title(),
        'icon': r['list'][i]['weather'][0]['icon'],
        'cloudness': r['list'][i]['clouds']['all'],
        'wind': r['list'][i]['wind']['speed'],
        'wind_deg':wind_deg(r['list'][i]['wind']['deg'])}
        weather_dict.append(new)
    return weather_dict

def days_get(weather_dict):
    global d0, d1, d2, d3, d4, d5, n0, n1, n2, n3, n4, n5
    # d0 and n0 are empty for that case if API return no day hours
    d0=[]; d1=[];d2=[];d3=[];d4=[];d5=[]
    n0=[]; n1=[];n2=[];n3=[];n4=[];n5=[]
    dc=1 # Day counter
    nc=1 # Night counter
    for i in range(len(weather_dict)):
        if weather_dict[i]['time'][0:2] in ["12","15","18"]:
            match dc:
                case 1: d1.append(weather_dict[i])
                case 2: d2.append(weather_dict[i])
                case 3: d3.append(weather_dict[i])
                case 4: d4.append(weather_dict[i])
                case 5: d5.append(weather_dict[i])
        elif weather_dict[i]['time'][0:2] in ["00","03"]:
            match nc:
                case 1: n1.append(weather_dict[i])
                case 2: n2.append(weather_dict[i])
                case 3: n3.append(weather_dict[i])
                case 4: n4.append(weather_dict[i])
                case 5: n5.append(weather_dict[i])
        if weather_dict[i]['time'][0:2] == "18": dc+=1
        if weather_dict[i]['time'][0:2] == "03": nc+=1
    empty_dict={'time':0, 'temp': 0, 'pressure': 0, 'humidity': 0, 'weather': 0, 'description': 0, 'icon': 0, 'cloudness': 0, 'wind': 0, 'wind_deg': 0}
    d0.append(empty_dict); n0=d0
# 5 дней - берём температуру среднюю по 12,15,18 и минимальную с 0,3
# Максимальное давление по 12,15,18 и 0,3
# Погоду в 15 и в 3, если в 12,15,18 (0,3) дождь, показать
# Ветер максимум из 12,15,18 и из 0,3 + направление
# days_get()
# daily_get(d1,n1) etc..

def daily_get(day,night):
    # Rain check
    if day == [] or night == []: return
    for i in range(len(day)):
        if day[i]['weather'] == "rain": d_weather=i
        else: d_weather=1
    for i in range(len(night)):
        if night[i]['weather'] == "rain": n_weather=i
        else: n_weather=1
    
    dp=[]; np=[]
    dt=[]; nt=[]
    for i in range(len(day)):
        dp.append(day[i]['pressure'])
        dt.append(day[i]['temp'])
    for i in range(len(night)):
        np.append(night[i]['pressure'])
        nt.append(night[i]['temp'])
    day_p=max(dp); night_p=max(np)
    day_t=min(dt); night_t=min(nt)

    if len(day)<2:
        d_weather=0
        d_wind=0
    else: d_wind=1
    if len(night)<2:
        n_weather=0
        n_wind=0
    else: n_wind=1
    if day[0]['time'] == 0:
        d_predict={
            "date": night[0]['time'].split(" | ")[1],
            "temp": "N/A",
            "pressure": "N/A",
            "weather": "N/A",
            "wind": "N/A"
        }
        n_predict={
            "date": night[0]['time'].split(" | ")[1],
            "temp": night_t,
            "pressure": night_p,
            "weather": night[n_weather]["description"],
            "wind": f"{night[n_wind]['wind']}{night[n_wind]['wind_deg']} mps"
        }
    else:
        d_predict={
            "date": day[0]['time'].split(" | ")[1],
            "temp": day_t,
            "pressure": day_p,
            "weather": day[d_weather]["description"],
            "wind": f"{day[d_wind]['wind']}{day[d_wind]['wind_deg']} mps"
        }
        n_predict={
            "date": night[0]['time'].split(" | ")[1],
            "temp": night_t,
            "pressure": night_p,
            "weather": night[n_weather]["description"],
            "wind": f"{night[n_wind]['wind']}{night[n_wind]['wind_deg']} mps"
        }
    
    print(f"""
Date: {d_predict['date']}
    Day:            Night:
    {d_predict['weather']}      {n_predict['weather']}
    {icons['Temp']} :{d_predict['temp']}°       {icons['Temp']} :{n_predict['temp']}°
    P:{d_predict['pressure']} hPa      P:{n_predict['pressure']} hPa
    {icons['Wind']}:{d_predict['wind']}     {icons['Wind']}:{n_predict['wind']}""")


# Прогноз на 3n часов
# Запрашиваем количество точек и выводим ту же информацию
def needed(need, weather_dict):
    for i in range(need):
        print(f"""Date: {weather_dict[i]['time']}
    {weather_dict[i]['description']}
    {icons['Temp']} :{weather_dict[i]['temp']}°
    P:{weather_dict[i]['pressure']} hPa
    {icons['Wind']}:{weather_dict[i]['wind']}{weather_dict[i]['wind_deg']} mps
    """)

what=input("""(1) - 5 Days forecast
(2) - 3n Hours forecast
(q) - Quit
""")
if what not in ["1","2"]: sys.exit()
r=forecast()
wd=w_dict(r)
match what:
    case "1":
        days_get(wd)
        if int(d1[0]['time'].split(" | ")[1].split("/")[0]) == int(n1[0]['time'].split(" | ")[1].split("/")[0]):
            daily_get(d0,n1)
            daily_get(d1,n2)
            daily_get(d2,n3)
            daily_get(d3,n4)
            daily_get(d4,n5)
        else:
            daily_get(d1,n1)
            daily_get(d2,n2)
            daily_get(d3,n3)
            daily_get(d4,n4)
            daily_get(d5,n5)
    case "2":
        need=int(input("Number of points: "))
        needed(need,wd)
    case "q": pass
    case _: print("Booba")
