import requests, os
from datetime import datetime

from variables import appid, load_data, save_data, icons, wind_deg, city_check

url="https://api.openweathermap.org/data/2.5/weather"
clear=lambda: os.system("clear")
clear()
def current():
    global r, timezone
    print("Input city")
    match input(": ").lower():
        case "load": r=load_data("data.json")
        case _ as city: 
            city_check(city)
            units="metric"
            params={"q":city, "units":units, "appid":appid}
            r = requests.get(url, params=params).json()
            save_data(r,"data.json")
    timezone=r["timezone"]

current()
weather={
    "description": r["weather"][0]["description"].title(),
    "icon": r["weather"][0]["icon"],
    "cloudness": r["clouds"]["all"]
}
parameters={
    "pressure": r["main"]["pressure"],
    "humidity": r["main"]["humidity"],
    "temperature": r["main"]["temp"]
}
wind={
    "speed": r["wind"]["speed"],
    "degree": r["wind"]["deg"]
}
sun={
    "sunrise": datetime.utcfromtimestamp(timezone+r["sys"]["sunrise"]).strftime("%H:%M"),
    "sunset": datetime.utcfromtimestamp(timezone+r["sys"]["sunset"]).strftime("%H:%M")
    }
save_data(r, "data.json")

print(f"{weather['description']}")
print(f"{icons['Temp']} : {parameters['temperature']}° Hum.: {parameters['humidity']}%")
print(f"{icons['Wind']} : {wind['speed']}{wind_deg(wind['degree'])} mps")
print(f"Sunrise at: {sun['sunrise']}\nSunset at: {sun['sunset']}")