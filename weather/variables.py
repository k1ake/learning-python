import json, sys, pandas as pd, subprocess
appid="71b04fb8e38281300753d286f65fb15a"
cities_csv = pd.read_csv("worldcities.csv")
cities=cities_csv["city"].tolist()

def city_check(city):
    if city.title() not in cities: 
        print("No such city"); sys.exit()
icons={
    "01d": "☀",
    "01n": "☾",
    "02d": "☁",
    "02n": "☁",
    "03d": "☁☁",
    "03n": "☁☁",
    "04d": "☁☁☁",
    "04n": "☁☁☁",
    "09d": "🌧",
    "09n": "🌧",
    "10d": "⛆",
    "10n": "⛆",
    "11d": "🌩",
    "11n": "🌩",
    "13d": "❄",
    "13n": "❄",
    "50d": "🌫",
    "50n": "🌫",
    "Hum": "💧",
    "Wind": "༄",
    "Temp": "🌡"
}
def load_data(file):
    with open(file) as f:
        response = json.load(f)
    return response
def save_data(response, file):
    with open(file,"w", encoding="utf-8") as f:
        json.dump(response, f)
    # subprocess.run(["notify-send","Saved"])
def wind_deg(deg):
    if deg >=22.5 and deg < 67.5: direction="NE"
    if deg >=67.5 and deg < 112.5: direction="E"
    if deg >=112.5 and deg < 157.5: direction="SE"
    if deg >=157.5 and deg < 202.5: direction="S"
    if deg >=202.5 and deg < 247.5: direction="SW"
    if deg >=247.5 and deg < 292.5: direction="W"
    if deg >=292.5 and deg < 377.5: direction="NW"
    else: direction="N"
    return direction
