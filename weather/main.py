from datetime import datetime
import requests, json, pandas as pd, sys, os

clear = lambda: os.system("clear")
clear()

match input("""(1) - Current weather
(2) - Forecast
: """):
    case "1": exec(open(f"current.py").read())
    case "2": exec(open(f"forecast.py").read())
    case _: pass