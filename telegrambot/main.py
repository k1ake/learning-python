# import asyncio
from aiogram import Bot, types, Dispatcher, executor
from pytube import YouTube

from pybeamit import JustBeamIt

import re

# ============
import config

bot = Bot(token=config.TOKEN)
dp = Dispatcher(bot)
resolutions = ["144p", "240p", "360p", "480p", "720p", "1080p"]


@dp.message_handler(commands=["start", "help", "download"])
async def send_welcome(msg: types.Message):
    check = False
    # Получаем команду
    command = msg.get_command()

    # Скачивание видео
    if command == "/download":
        args = msg.get_args().split(" ")
        if args[0] == "":
            print("Пусто!")
            await msg.answer(f"Команда требует ссылку для скачивания")
            return

        # Паттерны для ключей
        namepatt = r"^&N=.*"
        audiopatt = r"^&A.*"
        qualpatt = r"^&Q=.*"

        # Паттерны ссылок на видео
        patt1 = r"^(https://)?(www.)?youtube.com/watch\?v=.*"
        patt2 = r"^(https://)?(www.)?youtu.be/.*"

        # Получаем ссылку и аргументы
        for arg in args:
            if re.match(patt1, arg) or re.match(patt2, arg):
                url = arg
                check = True
        # Получение имени для видео
            elif re.match(namepatt, arg):
                name = arg[3:]
                print("Новое имя:", name)
        # Скачать только звук
            elif re.match(audiopatt, arg):
                audio = True
                print("Только звук:", audio)
        # Скачать в выбранном качестве
            elif re.match(qualpatt, arg):
                reso = arg[3:]
                if reso in resolutions: resolution = reso
                print("Разрешение:", resolution)
        else:
            if not check:
                await msg.answer(f"Ссылка не указана или ссылается не на ютуб!")
                return

        yt = YouTube(url)
        if "resolution" not in locals(): resolution = config.resolution
        if "audio" not in locals(): audio = config.audio

        try:
            streams = yt.streams
            if audio:
                print("Пытаемся в звук")
                stream = streams.get_audio_only()
                itag = int(str(stream).replace("<Stream: ", "").split(" ")[0][6:-1])
            else:
                print("Пытаемся в видео")
                stream = streams.filter(progressive=False, res=resolution)
                itag = int(str(stream[0]).replace("<Stream: ", "").split(" ")[0][6:-1])

        except: await msg.answer(f"Видео недоступно или не существует"); return
        else:
            stream = streams.get_by_itag(itag)
            if "name" not in locals():
                name = stream.default_filename

            video = {"name": name,
                     "title": stream.title,
                     "ext": stream.mime_type.split("/")[1],
                     "res": stream.resolution,
                     "type": stream.type,
                     "size": round(stream.filesize_approx / (1024 * 1024), 2),
                     "length": round((yt.length) / (60), 2),
                     }
            name = video["name"] + "." + video["ext"]
            print(video)
        await msg.answer(f"Скачиваем: {video['title']}")
        stream.download(filename=name)

        if video["size"] < 50:
            with open(name, "rb") as vid:
                try:
                    await msg.answer_document(vid)
                except: await msg.answer(f"Не поддерживается телеграммом")
        else:
            f = name
            j = JustBeamIt(f)
            t = j.tokenise()
            await msg.answer(f"Видео слишком большое для передачи через телеграм, вы можете \
скачать его к себе на устройство по ссылке ниже\n{t}")
            await msg.answer(f"Вы не сможете скачать другое видео, пока не скачаете текущее")
            print("Ждём скачивания")
            j.wait()
            j.transfer()
            print("done")
            await msg.answer(f"Спасибо за скачивание через наш бот!")

        # Help message
    elif command == "/help":
        await msg.answer("""
/help - Выведет текущее сообщение
/download [ссылка] (ключи) - скачает видео с ютуба по введённой ссылке с введённым именем

[] - обязательные поля

Дополнительные ключи:
&N=[Название] - задать название скачиваемому файлу
&A - Скачать звук из видео
&Q=[1080p/720p/480p/360p/240p/144p] - скачать в заданном качестве

Телеграм имеет ограничение на передачу файлов, размер которых превышает 50 Мб.\
Если файл видео/аудио окажется больше, мы предоставим вам ссылку для его скачивания.

Пока вы не скачаете файл или не отмените его загрузку таким путём, вы не сможете скачать следующий

Приблизительные ограничения на видео для скачивания через телеграм:
1080р : Не больше 3 минут
720р  : Не больше 10 минут
480р  : Не больше 15 минут
360р  : Не больше 20 минут
                  """)

    else: await msg.reply(f"Ты нажал {command}")


@dp.message_handler(content_types=["text"])
async def get_text_messages(msg: types.Message):
    # print(msg.text)
    if msg.text.lower() == "привет":
        await msg.answer("Хелоу")
    else:
        await msg.answer("Чё?")


if __name__ == "__main__":
    executor.start_polling(dp)
