# About
Simple bot that downloads video from youtube and sends it to you as a file, so you can download it to your device


# Dependencies
- pip install git+https://github.com/pytube/pytube 
- pip install pybeamit
- pip install aiogram
You need to create your bot with @BotFather in telegram and recieve bot-token, which should be placed in config.py in TOKEN variable

# How to use
* Run ```python main.py``` in your terminal to start the bot
* Start messaging with it in telegram

`/start` or `/help` will give you all info about bot usage
