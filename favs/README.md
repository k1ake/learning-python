# Favs

Saves your paths and files so you can easilly come back to them

## Dependencies
```
python-magick

```

## Installation
Run 
```
wget --no-check-certificate --content-disposition https://gitlab.com/k1ake/learning-python/-/raw/main/favs/favs
chmod +x favs
mv favs ~/.local/bin/
```
or build it yourself with pythom

## Before usage
run ```favs``` to create config file. it will be placed in .config/favs/    

make sure to change defaults if you have not everything from that list:
- file manager: thunar
- media player: vlc
- text editor: micro 

## Usage
run ```favs -h``` for help
- ```favs -a PATH/FILE``` to save it
- ```favs -R``` to remove with TUI or ```favs -r PATH``` to remove with CLI
- ```favs -l```, ```favs -p``` or ```favs -f``` to print saved everything/paths/files
- ```favs -o``` to open saved path/file   

Add ```-v``` to increase output or ```-q``` to suppress it
