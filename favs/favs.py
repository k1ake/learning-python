import os, sys, subprocess, argparse, magic, numpy as np, configparser

# Команда записывает путь/файл в файл в избранное
# аргументы:
# [] - show help
# -l - list everything
# -p - show saved paths
# -f - show saved files
# -a PATH/FILE - add path/file to favorites
# -r PATH/FILE - remove path/file from favorites
# -o - open PATH/FILE from favorites
# -v(vv) - verbosity level
# -q - supress output


#   ____ ___  _   _ _____ ___ ____
#  / ___/ _ \| \ | |  ___|_ _/ ___|
# | |  | | | |  \| | |_   | | |  _
# | |__| |_| | |\  |  _|  | | |_| |
#  \____\___/|_| \_|_|   |___\____|

config_path = os.path.expanduser("~") + "/.config/favs/"
config_name = "config.ini"
config = configparser.ConfigParser()
if not os.path.exists(config_path):
    os.mkdir(config_path)
if not os.path.exists(config_path + config_name):
    config["EDITORS"] = {'file_manager': 'thunar',
                         'text_editor': 'micro',
                         'music_player': 'vlc',
                         'video_player': 'vlc',
                         'picture_viewer': 'feh'}
    with open(config_path + config_name, 'w') as configfile:
        config.write(configfile)
config.read(config_path + config_name)
fm = config["EDITORS"]["file_manager"]
te = config["EDITORS"]["text_editor"]
mp = config["EDITORS"]["music_player"]
vp = config["EDITORS"]["video_player"]
pic = config["EDITORS"]["picture_viewer"]

#  ____    ___     _______ ____
# / ___|  / \ \   / / ____/ ___|
# \___ \ / _ \ \ / /|  _| \___ \
#  ___) / ___ \ V / | |___ ___) |
# |____/_/   \_\_/  |_____|____/

saves = config_path + "saves.npz"
if not os.path.exists(saves):
    list_p = []
    list_f = []
    np.savez(saves, paths=list_p, files=list_f)
a = np.load(saves)
list_p = a['paths'].tolist()
list_f = a['files'].tolist()


#  _____ _   _ _   _  ____ _____ ___ ___  _   _ ____
# |  ___| | | | \ | |/ ___|_   _|_ _/ _ \| \ | / ___|
# | |_  | | | |  \| | |     | |  | | | | |  \| \___ \
# |  _| | |_| | |\  | |___  | |  | | |_| | |\  |___) |
# |_|    \___/|_| \_|\____| |_| |___\___/|_| \_|____/

def short_path(p):
    P = p.split("/")
    P = [new for new in P if "" != new]
    if P == []: return p
    for i in range(len(P)):
        if i != len(P) - 1 and len(P[i]) > 1:
            P[i] = P[i][:1] + "/"
    p = "/" + "".join(P) + "/"
    return p


def short_file(f):
    F = f.split("/")
    F = [new for new in F if "" != new]
    if F == []: return f
    for i in range(len(F)):
        if i != len(F) - 1 and len(F[i]) > 1:
            F[i] = F[i][:1] + "/"
    f = "/" + "".join(F)
    return f


def print_all():
    if verb <= 1:
        print("Paths:")
        for i in list_p: print(f"  {short_path(i)}")
        print("Files:")
        for i in list_f: print(f"  {short_file(i)}")
    elif verb > 1:
        print("Paths:")
        for i in list_p: print(f"  {i}")
        print("Files:")
        for i in list_f: print(f"  {i}")


def print_p():
    if verb <= 1:
        print("Paths:")
        for i in list_p: print(f"  {short_path(i)}")
    elif verb > 1:
        print("Files:")
        for i in list_p: print(f"  {i}")


def print_f():
    if verb <= 1:
        print("Files:")
        for i in list_f: print(f"  {os.path.basename(i)}")
    elif verb > 1:
        print("Files:")
        for i in list_f: print(f"  {i}")


def add_p():
    if "/" + os.path.relpath(args.add, start="/") + "/" == "/./":
        list_p.append("/")
    else: list_p.append("/" + os.path.relpath(args.add, start="/") + "/")
    if verb == 1:
        print(f"{args.add} added to favorite paths")
    if verb > 1:
        print(f"{args.add} added to favorite paths")
        print(f"Saved paths:")
        for i in list_p: print(f"{i}")


def add_f():
    list_f.append("/" + os.path.relpath(args.add, start="/"))
    if verb == 1:
        print(f"{args.add} added to favorite files")
    elif verb > 1:
        print(f"{args.add} added to favorite files")
        print(f"Saved files:")
        for i in list_f: print(f"{i}")


def remove_p():
    if os.path.relpath(args.remove, start="/") == ".":
        list_p.remove("/")
    else:
        list_p.remove("/" + os.path.relpath(args.remove, start="/") + "/")

    if verb == 1:
        print(f"{args.remove} removed from favorite paths")
    elif verb > 1:
        print(f"{args.remove} removed from favorite paths")
        print(f"Saved paths:\n{list_p}")


def remove_f():
    list_f.remove("/" + os.path.relpath(args.remove, start="/"))

    if verb == 1:
        print(f"{args.remove} removed from favorite files")
    elif verb > 1:
        print(f"{args.remove} removed from favorite files")
        print(f"Saved files:\n{list_f}")


def remove_none():
    if verb == 1:
        print(f"{args.remove} not in favorites")
    elif verb >= 1:
        print(f"{args.remove} not in favorites\nSaved paths:")
        for i in list_p:
            print(f"{i}")
        print(f"Saved files:")
        for i in list_f:
            print(f"{i}")


def remove_manually():
    remove_fav = int(input(f"(1) to remove path; (2) to remove file\n:"))
    match remove_fav:
        case 1:
            print("Saved paths:")
            for i in range(len(list_p)):
                print(f"[{i+1}] {list_p[i]}")
            path = int(input("Remove: ")) - 1
            try: list_p.remove(list_p[path])
            except IndexError: pass
            except ValueError: pass
            if verb >= 1:
                print_p()
            else: pass
        case 2:
            print("Saved files:")
            for i in range(len(list_f)):
                print(f"[{i+1}] {list_f[i]}")
            file = int(input("Remove: ")) - 1
            try: list_f.remove(list_f[file])
            except IndexError: pass
            if verb >= 1:
                print_f()
            else: pass


def open_with():
    open_fav = input(f"(1) to open path; (2) to open file\n:")
    match open_fav:
        case "1":
            print("Saved paths:")
            for i in range(len(list_p)):
                print(f"[{i+1}] {list_p[i]}")
            path = int(input("Go to: ")) - 1
            try: subprocess.run([fm, list_p[path]])
            except IndexError: print("No path with that index")
        case "2":
            print("Saved files:")
            for i in range(len(list_f)):
                print(f"[{i+1}] {list_f[i]}")
            file = int(input("Open: ")) - 1
            if file >= len(list_f) or file < 1:
                print("No file with that index")
            ftype = magic.from_file(list_f[file], mime=True)
            match ftype.split("/")[0]:
                case "text": subprocess.run([te, list_f[file]])
                case "video": subprocess.run([vp, list_f[file]])
                case "audio": subprocess.run([mp, list_f[file]])
                case "application":
                    if ftype.split("/")[1] != "x-pie-executable":
                        subprocess.run(["xdg-open", list_f[file]])
                    else: subprocess.run([list_f[file]])
                case _: print("That type is not compatible")
        case _: print("")
#     _    ____   ____ _   _ __  __ _____ _   _ _____ ____
#    / \  |  _ \ / ___| | | |  \/  | ____| \ | |_   _/ ___|
#   / _ \ | |_) | |  _| | | | |\/| |  _| |  \| | | | \___ \
#  / ___ \|  _ <| |_| | |_| | |  | | |___| |\  | | |  ___) |
# /_/   \_\_| \_\\____|\___/|_|  |_|_____|_| \_| |_| |____/


parser = argparse.ArgumentParser(description="Saves paths and files to faster open and modify them")

# Делим аргументы на группы
lists = parser.add_mutually_exclusive_group()
verbosity = parser.add_mutually_exclusive_group()
manipulation = parser.add_mutually_exclusive_group()

# Отвечают за вывод
lists.add_argument("-l", "--list", help="Print everything added to favorites", action="count", default=0)
lists.add_argument("-p", "--list-paths", help="Print list of saved paths", action="count", default=0)
lists.add_argument("-f", "--list-files", help="Print list of saved files", action="count", default=0)

# Отвечают за работу с избранным
manipulation.add_argument("-a", "--add", help="Add file to favorites", type=str, default="")
manipulation.add_argument("-r", "--remove", help="Remove path/file from list", type=str, default="")
manipulation.add_argument("-R", "--remove-manually",
                          help="Remove path/file from list with TUI", action="count", default=0)
manipulation.add_argument("-o", "--open", help="cd in path or open file", default=0, action="count")

# Отвечают за вывод
verbosity.add_argument("-v", "--verbose", help="Verbose level", default=1, action="count")
verbosity.add_argument("-q", "--quiet", help="Supress output", default=0, action="count")

args = parser.parse_args()

# Display usage if no args given
if len(sys.argv) < 2:
    parser.print_usage()
    sys.exit()

if args.quiet > 0:
    verb = 0
elif args.verbose == 1:
    verb = 1
elif args.verbose > 1:
    verb = 2

#  ____  _   _  _____        _____ _   _  ____
# / ___|| | | |/ _ \ \      / /_ _| \ | |/ ___|
# \___ \| |_| | | | \ \ /\ / / | ||  \| | |  _
#  ___) |  _  | |_| |\ V  V /  | || |\  | |_| |
# |____/|_| |_|\___/  \_/\_/  |___|_| \_|\____|

if args.list > 0:
    print_all()
    if len(sys.argv) > 2:
        sys.exit()
elif args.list_paths > 0:
    print_p()
elif args.list_files > 0:
    print_f()

#     _    ____  ____ ___ _   _  ____
#    / \  |  _ \|  _ \_ _| \ | |/ ___|
#   / _ \ | | | | | | | ||  \| | |  _
#  / ___ \| |_| | |_| | || |\  | |_| |
# /_/   \_\____/|____/___|_| \_|\____|

if os.path.isdir(args.add) and "/" + os.path.relpath(args.add, start="/") + "/" not in list_p:
    add_p()
elif os.path.isfile(args.add) and "/" + os.path.relpath(args.add, start="/") not in list_f:
    add_f()
elif args.add != "" and os.path.exists("/" + os.path.relpath(args.add, start="/")):
    print(f"{args.add} Already in favorites")
elif args.add != "": print("File or path do not exist")

#  ____  _____ __  __  _____     _____ _   _  ____
# |  _ \| ____|  \/  |/ _ \ \   / /_ _| \ | |/ ___|
# | |_) |  _| | |\/| | | | \ \ / / | ||  \| | |  _
# |  _ <| |___| |  | | |_| |\ V /  | || |\  | |_| |
# |_| \_\_____|_|  |_|\___/  \_/  |___|_| \_|\____|
# print("/"+os.path.relpath(args.remove, start="/"))
if os.path.isdir(args.remove) and args.remove in list_p:
    remove_p()
elif os.path.isfile(args.remove) and "/" + os.path.relpath(args.remove, start="/") in list_f:
    remove_f()
elif args.remove != "":
    remove_none()
elif args.remove_manually > 0:
    remove_manually()

#   ___  ____  _____ _   _ _   _ ___ _   _  ____
#  / _ \|  _ \| ____| \ | | \ | |_ _| \ | |/ ___|
# | | | | |_) |  _| |  \| |  \| || ||  \| | |  _
# | |_| |  __/| |___| |\  | |\  || || |\  | |_| |
#  \___/|_|   |_____|_| \_|_| \_|___|_| \_|\____|

if args.open > 0:
    open_with()

np.savez(saves, paths=list_p, files=list_f)
