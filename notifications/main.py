from logging import critical
import gi, os, subprocess
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk as gtk

class Main:
    def __init__(self) -> None:
        gladefile = "window.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladefile)
        self.builder.connect_signals(self)

        window = self.builder.get_object("main")
        window.connect("delete-event", gtk.main_quit)
        window.show()


    def urg_crit(self, radiobutton):
        global urg
        urg="critical"

    def urg_norm(self, radiobutton):
        global urg
        urg="normal"

    def urg_low(self, radiobutton):
        global urg
        urg="low"

    def send_notification(self,widget):
        bodyInput = self.builder.get_object("Body")
        titleInput = self.builder.get_object("Title")

        body = bodyInput.get_text().strip()
        title = titleInput.get_text().strip()
        noti=["notify-send",title,body,"-u",urg]
        subprocess.run(noti)
        bodyInput.set_text("")
        titleInput.set_text("")

if __name__ == "__main__":
    os.system("clear")
    main = Main()
    gtk.main()
