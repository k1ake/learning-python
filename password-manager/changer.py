from globals import files
import json, codecs


def deleter(name=None, check=False):
    if name == None:
        name = input("Input password's alias to remove: ")
        confirm = input("Input alias again for confimation: ")
        if name != confirm: print("They are different\nAborting.."); return
    file = files()
    l = []
    if check:
        confirm = input("Input alias again for confimation: ")
        if name != confirm: print("They are different\nAborting.."); return
    with open(file) as f:
        tmp = json.load(f)
    for i in range(0, len(tmp)):
        if codecs.decode(tmp[i]['name'], 'hex').decode('utf-8') != name:
            l.append(tmp[i])
    with open(file, 'w') as f:
        json.dump(l, f)
