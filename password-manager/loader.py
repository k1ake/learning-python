import sys, codecs, json
from Crypto.Cipher import AES as aes

from globals import files


def restore(codename=None):
    if codename == None: codename = input("Input codename: ")
    codename = codename.lower().encode("utf-8")
    file = files()
    dict = None
    with open(file) as f:
        tmp = json.load(f)
    for i in range(len(tmp)):
        if codecs.encode(codename, 'hex').decode() == tmp[i]['name']:
            dict = tmp[i]
    if dict == None: print("No such codename in base, aborting"), sys.exit()
    # Restoring key
    # kkey=codecs.encode(dict['key'],'hex').decode()
    kkey = dict['key']
    k1 = list(kkey)[:len(kkey) // 2]
    k2 = list(kkey)[len(kkey) // 2:]
    key = codecs.decode(''.join(k2 + k1), 'hex')

    # Restoring txt
    # txt=codecs.encode(dict['txt'],'hex').decode()
    txt = dict['txt']
    o = txt[-2:] + txt[2:-2] + txt[0:2][::-1]
    txt = codecs.decode(o, 'hex')

    # Restoring nonce
    # nonce=codecs.encode(dict['non'], 'hex').decode()
    nonce = dict['non']
    old = ''
    for i in range(len(nonce) // 2):
        old += nonce[2 * i + 1] + nonce[2 * i]
    nonce = codecs.decode(old, 'hex')

    # Restoring tag
    # tag=list(codecs.encode(dict['tag'], 'hex').decode())
    tag = dict['tag']
    l = len(tag) // 4
    o = ''.join(tag[3 * l:] + tag[l:3 * l] + tag[:l])
    tag = codecs.decode(o, 'hex')

    # Building dict
    odict = {
        'name': codename.decode('utf-8'),
        'key': key,
        "txt": txt,
        "non": nonce,
        "tag": tag,
    }
    return odict


def decrypt(dict):
    # Decryption needs key, ciphertext, cipher.nonce and tag to decrypt
    cipherO = aes.new(dict['key'], aes.MODE_EAX, dict['non'])
    dataO = cipherO.decrypt_and_verify(dict['txt'], dict['tag'])
    # pyperclip.copy(dataO.decode('utf-8'))
    name = dict['name']
    return dataO.decode('utf-8'), name
