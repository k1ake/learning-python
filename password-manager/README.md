# Password-manager

## Requirements:
pip: `pycryptodome, pyperclip`

## Usage:

- `python passman --gui` - run GUI version

- `python passman -l` - Print all saved passwords
- `python passman -s <alias>` - Show password under that alias
- `python passman -c <alias>` - Copy password under that alias
- `python passman -a <alias>` - Save new password 
- `python passman -r <alias>` - Remove password
- `python passman -C <alias>` - Change password and keep previous alias
- `python passman -g <lvl> <length>` - Generate password with that security level and length

Verbosity levels could be adjusted with `-v` or `-q` flags

In CLI mode first usage will ask you to create master password and exit after that
Every command will ask for master password generation will ask master password


Known bugs:
- Can not run same window twice, it's blank and that error appears in terminal:   

`Gtk-CRITICAL **: 12:29:29.771: gtk_container_foreach: assertion 'GTK_IS_CONTAINER (container)' failed`

Got no clue how to deal with it :P 
