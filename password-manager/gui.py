# PYTHON IMPORTS
import os, subprocess
import sys

# GTK IMPORTS
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk as gtk, Gdk as gdk

# FILES IMPORTS
from lister import lister
from loader import decrypt, restore
from saver import encrypt, store
from changer import deleter
from globals import master_pass, files
from passgen import new_password


# ТЕКУЩИЕ ПРОБЛЕМЫ
# ТЕКУЩИЕ ПРОБЛЕМЫ

# ПОВТОРНОЕ ОТКРЫТИЕ КНОПКИ, КРАШИТ ОКОШКО :D

# ТЕКУЩИЕ ПРОБЛЕМЫ
# ТЕКУЩИЕ ПРОБЛЕМЫ

class Main:
    def __init__(self) -> None:
        gladefile = "window.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladefile)
        self.builder.connect_signals(self)

        # Master password
        self.mastercheck()
        self.wrongmaster = self.builder.get_object('wrongmaster')
        master.show()

        # Variables
        self.selected = None
        self.stored = []

        # List of passwords
        self.passtreeview = self.builder.get_object("passtreeview")
        self.passlist = gtk.ListStore(str)
        self.setuplist()
        sel = self.passtreeview.get_selection()
        sel.connect("changed", self.treesel)

        # Clipboard
        self.clipboard = gtk.Clipboard.get(gdk.SELECTION_CLIPBOARD)
        gtk.Clipboard.clear(self.clipboard)

        # New password
        self.storenew = self.builder.get_object('addpasswindow')
        self.NPsuccess = self.builder.get_object('NPsuccess')
        self.NPalias = self.builder.get_object('NPalias')

        # Change password
        self.changepass = self.builder.get_object('changepasswindow')
        self.changelabel = self.builder.get_object('changelabel')

        # Delete password
        self.delpass = self.builder.get_object('deletewindow')
        self.dellabel = self.builder.get_object('dellabel')

        # Generator
        self.genlabel = self.builder.get_object('genereatedpasslabel')

        window = self.builder.get_object("main")
        window.connect("delete-event", gtk.main_quit)
        window.show()

    def treesel(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            # print("You selected", model[treeiter][0])
            self.selected = treeiter

    def copypop(self, widget):
        tocopy = self.passlist.get_value(self.selected, 0)
        restored = decrypt(restore(tocopy))
        self.clipboard.set_text(restored[0], -1)
        subprocess.run(["notify-send", restored[1], "Saved to clipboard"])

    def newpop(self, widget):  # Adding new password
        self.storenew.show()

    def NPcancel(self, widget):
        # self.storenew.close()
        self.storenew.destroy()

    def NPconfirm(self, widget):
        aliasinput = self.builder.get_object("NPalias")
        passinput = self.builder.get_object("NPpass")
        passcinput = self.builder.get_object("NPpassconfirm")
        alias = aliasinput.get_text().strip().lower()
        newpass = passinput.get_text().strip()
        confirm = passcinput.get_text().strip()

        if alias == "":
            subprocess.run(["notify-send", "-u", "critical", "Alias is empty!"])
            return
        elif alias in self.stored:
            subprocess.run(["notify-send", "-u", "critical", "Alias already in use"])
            return

        if newpass != confirm and newpass != "":
            subprocess.run(["notify-send", "-u", "critical", "PASSWORDS DO NOT MATCH"])
            return
        elif newpass == confirm and newpass == "":
            subprocess.run(["notify-send", "-u", "critical", "PASSWORDS ARE EMPTY"])
            return

        store(encrypt(newpass, alias))
        subprocess.run(["notify-send", "", "Password stored successfully"])
        self.passlist.append([alias])

        self.storenew.close()

    def delpop(self, widget):  # Deleting stored password
        todel = self.passlist.get_value(self.selected, 0)
        markup = "Are you sure you want to delete " + "<big>" + \
            todel + "</big>" + "?" + '\n' +\
            "Input " + "<span size='x-large' color='red'><u>" + todel + "</u></span>" + " in field below"
        self.dellabel.set_markup(markup)
        self.delpass.show()

    def Dcancel(self, widget):
        self.delpass.close()

    def Dconfirm(self, widget):
        todel = self.passlist.get_value(self.selected, 0)
        confirmationEntry = self.builder.get_object("Dentry")
        confirmation = confirmationEntry.get_text().strip().lower()
        if confirmation == todel:
            deleter(confirmation)
            self.passlist.remove(self.selected)
            self.delpass.close()
        else:
            markup = "<span color='red' size='xx-large'>Incorrect!</span>\n" + \
                "Input " + "<span size='x-large' color='red'><u>" + todel + "</u></span>" + " in field below"
            self.dellabel.set_markup(markup)

    def chgpop(self, widget):  # Changing stored password
        markup = "New pass will be stored under " + "<big>" + \
            self.passlist.get_value(self.selected, 0) + "</big>" + " alias"
        self.changelabel.set_markup(markup)
        self.changepass.show()

    def CHcancel(self, widget):
        self.changepass.close()

    def CHconfirm(self, widget):
        tochange = self.passlist.get_value(self.selected, 0)
        passinput = self.builder.get_object("CHpass")
        passcinput = self.builder.get_object("CHpassconfirm")

        newpass = passinput.get_text().strip()
        confirm = passcinput.get_text().strip()

        if newpass != confirm and newpass != "":
            subprocess.run(["notify-send", "-u", "critical", "PASSWORDS DO NOT MATCH"])
            return
        elif newpass == confirm and newpass == "":
            subprocess.run(["notify-send", "-u", "critical", "PASSWORDS ARE EMPTY"])
            return

        deleter(tochange)
        store(encrypt(newpass, tochange))
        subprocess.run(["notify-send", "", "Password stored successfully"])
        self.passlist.remove(self.selected)
        self.passlist.append([tochange])
        self.changepass.close()

    def loadpass(self, widget):  # Loading saved passwords
        self.stored = lister()
        if self.stored == False:
            self.passlist.append([])
            return
        for Pass in self.stored:
            self.passlist.append([Pass])

    def setuplist(self):  # Creating list of passwords
        renderer = gtk.CellRendererText()
        passColumn = gtk.TreeViewColumn(title="Stored passwords:", cell_renderer=renderer, text=0)
        self.passtreeview.append_column(passColumn)
        self.passtreeview.set_model(self.passlist)

    def mastercheck(self):
        file = files()
        global master
        if not os.path.isfile(file):
            master = self.builder.get_object("newmaster")
        else: master = self.builder.get_object("masterwindow")

    def MNcancel(self, widget):
        sys.exit()

    def MNconfirm(self, widget):
        passinput = self.builder.get_object("Mpass")
        passcinput = self.builder.get_object("Mpasscheck")

        newpass = passinput.get_text().strip()
        confirm = passcinput.get_text().strip()

        if newpass != confirm and newpass != "":
            print("Different!")
            markup = "<span color='red' size='large'>Passwords are not same</span>\n"
            self.wrongmaster.set_markup(markup)

        elif newpass == "" or confirm == "":
            print("Empty!")
            markup = "<span color='red' size='large'>Password should not be empty</span>\n"
            self.wrongmaster.set_markup(markup)
        else:
            master_pass(newpass, newpass)
            self.passlist.clear()
            self.stored = lister()
            master.close()

    def mpop(self):
        self.masterwindow.show()

    def Mcancel(self, widget):
        sys.exit()

    def Mconfirm(self, widget):
        passinput = self.builder.get_object("masterentry")
        mpass = passinput.get_text().strip()
        master_pass(mpass)
        self.passlist.clear()
        self.stored = lister()
        self.loadpass(self.passlist)
        master.close()

    def Ggen(self, widget):
        genlvl = self.builder.get_object("genpassspin")
        lvl = int(genlvl.get_text().strip())
        genlen = self.builder.get_object("genpasslength")
        length = int(genlen.get_text().strip())

        if length > 13: newstr = "\n"
        else: newstr = ""
        self.password = new_password(length, lvl)
        markup = "<span size='large'> Generated password:</span> " + newstr + \
            "<span color='red' size='x-large'>" + self.password + "</span>"
        self.genlabel.set_markup(markup)

    def Gcopy(self, widget):
        self.clipboard.set_text(self.password, -1)


# if __name__ == '__main__':
#     main = Main()
#     gtk.main()
