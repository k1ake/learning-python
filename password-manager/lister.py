from globals import files
import json, codecs, os


def lister():
    file = files()
    if not os.path.isfile(file):
        return False
    l = []
    with open(file) as f:
        tmp = json.load(f)
    for i in range(1, len(tmp)):
        l.append(codecs.decode(tmp[i]['name'], 'hex').decode('utf-8'))
    return l
