import os, pwd, hashlib, codecs, json, subprocess, getpass, sys, random, string

lower = list(string.ascii_lowercase)
upper = list(string.ascii_uppercase)
digits = list(string.digits)
extra = [".", ",", "!", "#", "$", "%", ":", "_", "-"]
forbidden = list(string.punctuation)
for i in extra:
    forbidden.remove(i)
forbidden.append(" ")


def files():
    username = pwd.getpwuid(os.getuid())[0]
    l = len(username) // 2
    filename = (username[:l:-1] + username).encode('utf-8')
    file = "." + hashlib.md5(filename).hexdigest()
    return file


def master_pass(pass1=None, pass2=None):
    file = files()
    if not os.path.isfile(file):
        if pass1 == None and pass2 == None:
            pass1 = getpass.getpass("Create your master password: ")
            pass2 = getpass.getpass("Confirm your master password: ")
        if pass1 != pass2:
            print("Passwords are different. Aborting..")
            sys.exit()
        l = len(pass1) // 2
        master = pass1[len(pass1):l:-1] + pass1[l::-1]
        master = hashlib.md5(master.encode('utf-8')).hexdigest()
        name = codecs.encode("BigChungus69".encode('utf-8'), 'hex').decode()
        key = random.randint(10**15, 10**16 - 1)
        nonce = codecs.encode("f;ohF*4h;33F4%^sd;".encode('utf-8'), 'hex').decode()
        tag = codecs.encode("fasdf4f$FF4%^sd;".encode('utf-8'), 'hex').decode()
        masterdict = {
            'name': name,
            'key': key,
            'txt': master,
            'non': nonce,
            'tag': tag,
        }
        subprocess.run(["touch", file])
        tmp = [masterdict]
        with open(file, 'w') as f:
            json.dump(tmp, f)
    else:
        with open(file) as f:
            tmp = json.load(f)
        mcheck = tmp[0]['txt']
        if pass1 == None:
            pass1 = getpass.getpass("Input masterpassword: ")
        l = len(pass1) // 2
        passcheck = pass1[len(pass1):l:-1] + pass1[l::-1]
        passcheck = hashlib.md5(passcheck.encode('utf-8')).hexdigest()
        if mcheck != passcheck:
            print("Different passwords!")
            subprocess.run(["notify-send", "Master password is incorrect"])
            sys.exit()
        else: print("Same!")
