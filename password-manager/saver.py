import sys, codecs, getpass, json
from Crypto.Cipher import AES as aes
from Crypto.Random import get_random_bytes as grb

from globals import files


def store(dict):
    file = files()
    # Key storing
    skey = codecs.encode(dict['key'], 'hex').decode()
    s1 = list(skey)[:len(skey) // 2]
    s2 = list(skey)[len(skey) // 2:]
    skey = ''.join(s2 + s1)

    # Txt storing
    txt = codecs.encode(dict['txt'], 'hex').decode()
    n = txt[-2:][::-1] + txt[2:-2] + txt[0:2]
    txt = n
    # Nonce storing
    nonce = codecs.encode(dict['non'], 'hex').decode()
    new = ''
    for i in range(len(nonce) // 2):
        new += nonce[2 * i + 1] + nonce[2 * i]
    nonce = new

    # Tag storing
    tag = list(codecs.encode(dict['tag'], 'hex').decode())
    l = len(tag) // 4
    n = ''.join(tag[3 * l:] + tag[l:3 * l] + tag[:l])
    tag = n

    # Creating dict to store
    ndict = {
        "name": dict['name'],
        "key": skey,
        "txt": txt,
        "non": nonce,
        "tag": tag,
    }
    with open(file) as f:
        tmp = json.load(f)
    if len(tmp) > 0:
        repl = False
        for i in range(len(tmp)):
            if ndict['name'] == tmp[i]['name']:
                # print("That alias is choosen.")
                # match input("Do you want to replace it? (y/n)"):
                #     case "y" | "Y":
                #         print("Replacing...")
                #         tmp[i] = ndict
                #         repl = True
                #     case _: print("Aborting.."); sys.exit()
                sys.exit()
        if repl == False:
            tmp.append(ndict)
    with open(file, 'w') as f:
        json.dump(tmp, f)


def encrypt(data=None, name=None):
    # Encryption, need to store key, ciphertext, cipher.nonce and tag to decrypt
    if name == None:
        name = input("Enter codename: ").lower()
    if data == None:
        while True:
            data = getpass.getpass("Enter password to store: ")
            confirm = getpass.getpass("Confirm your password: ")
            if data == confirm: break
            print("Passwords are different")
    name = name.encode("utf-8")
    name = codecs.encode(name, 'hex').decode()
    data = data.encode("utf-8")
    key = grb(16)
    cipher = aes.new(key, aes.MODE_EAX)
    ciphertext, tag = cipher.encrypt_and_digest(data)
    dict = {
        'name': name,
        'key': key,
        'txt': ciphertext,
        'non': cipher.nonce,
        'tag': tag,
    }
    return dict


# store(encrypt())
# r=restore("piska")
# new_data=decrypt(r)
# print(new_data)
