import os, sys, argparse, pyperclip

from globals import master_pass, files
from loader import decrypt, restore
from passgen import new_password
from saver import encrypt, store
from changer import deleter
from lister import lister


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk as gtk, Gdk as gdk

from gui import Main

#     _    ____   ____ _   _ __  __ _____ _   _ _____ ____
#    / \  |  _ \ / ___| | | |  \/  | ____| \ | |_   _/ ___|
#   / _ \ | |_) | |  _| | | | |\/| |  _| |  \| | | | \___ \
#  / ___ \|  _ <| |_| | |_| | |  | | |___| |\  | | |  ___) |
# /_/   \_\_| \_\\____|\___/|_|  |_|_____|_| \_| |_| |____/


parser = argparse.ArgumentParser(description="Some description")

listing = parser.add_mutually_exclusive_group()
manipulation = parser.add_mutually_exclusive_group()
verbosity = parser.add_mutually_exclusive_group()
gui = parser.add_mutually_exclusive_group()


# Showing saved passwords
listing.add_argument("-l", "--list", help="Add password", default=0, action="count")
listing.add_argument("-s", "--show", nargs=1, metavar=("X"),
                     default="", type=str, help="Show password under alias")
listing.add_argument("-c", "--copy", nargs=1, metavar=("X"), default="",
                     type=str, help="Copy password under alias to your clipboard")


# Manipulations with passwords
manipulation.add_argument("-a", "--add", nargs=1, metavar=("X"), default="", type=str, help="Add password")
manipulation.add_argument("-r", "--remove", nargs=1, metavar=("X"),
                          default="", type=str, help="Remove password")
manipulation.add_argument("-C", "--change", nargs=1, metavar=("X"), default="", type=str, help="Change password")
manipulation.add_argument("-g", "--generate", nargs=2, metavar=("X", "L"), default=0, type=int,
                          help="Generate password with X - complexity level (0-3) and L - password length (4-100)")


# Verbosity
verbosity.add_argument("-v", "--verbose", help="Verbose level", default=1, action="count")
verbosity.add_argument("-q", "--quiet", help="Supress output", default=0, action="count")

# Interface
verbosity.add_argument("--gui", help="launch GUI", default=0, action="count")

args = parser.parse_args()

# Check if args given
if len(sys.argv) < 2:
    if not os.path.isfile(files()): master_pass()
    else: parser.print_usage()
    sys.exit()

if args.gui:
    main = Main()
    gtk.main()
    sys.exit

# Verbosity level
if args.quiet > 0:
    verb = 0
elif args.verbose == 1:
    verb = 1
elif args.verbose > 1:
    verb = 2

#  ____  _   _  _____        __
# / ___|| | | |/ _ \ \      / /
# \___ \| |_| | | | \ \ /\ / /
#  ___) |  _  | |_| |\ V  V /
# |____/|_| |_|\___/  \_/\_/

#  ____   _    ____ ______        _____  ____  ____  ____
# |  _ \ / \  / ___/ ___\ \      / / _ \|  _ \|  _ \/ ___|
# | |_) / _ \ \___ \___ \\ \ /\ / / | | | |_) | | | \___ \
# |  __/ ___ \ ___) |__) |\ V  V /| |_| |  _ <| |_| |___) |
# |_| /_/   \_\____/____/  \_/\_/  \___/|_| \_\____/|____/

if args.list > 0:
    master_pass()
    l = lister()  # Returns false if nothing to show
    if l != False:
        for i in range(len(l)): print(f"{i+1}) {l[i]}")
        if verb == 2: print("Total saved passwords:", len(l))
    else: print("No passwords stored")
    if len(sys.argv) > 2:
        sys.exit()

elif args.show:
    master_pass()
    toshow = decrypt(restore(args.show[0]))
    if verb < 2:
        print("Password:", toshow[0])
    else:
        print(f"Alias: {toshow[1]}\nPassword: {toshow[0]}")
    sys.exit()

elif args.copy:
    master_pass()
    pyperclip.copy(decrypt(restore(args.copy[0]))[0])
    if verb == 1: print("Password copied to your clipboard")
    elif verb == 2: print(f"Password under alias '{args.copy[0].upper()}' saved to your clipboard")

#     _    ____  ____ ___ _   _  ____
#    / \  |  _ \|  _ \_ _| \ | |/ ___|
#   / _ \ | | | | | | | ||  \| | |  _
#  / ___ \| |_| | |_| | || |\  | |_| |
# /_/   \_\____/|____/___|_| \_|\____|

if args.add:
    master_pass()
    l = lister()
    alias = args.add[0]

    if alias in l:
        while True:
            print("Alias is already taken")
            match input("Choose another alias? (y/n): "):
                case "y" | "Y":
                    alias = input("Input alias: ")
                    if alias not in l: break
                case _:
                    if verb == 2: print("You can change password under that alias with -c flag",
                                        args.add[0], "\nAborting..")
                    elif verb == 1: print("Aborting..")
                    sys.exit()

    store(encrypt(name=alias))
    if verb > 0: print(f"Password stored under '{alias}'")
    sys.exit()
#  ____  _____ __  __  _____     _____ _   _  ____
# |  _ \| ____|  \/  |/ _ \ \   / /_ _| \ | |/ ___|
# | |_) |  _| | |\/| | | | \ \ / / | ||  \| | |  _
# |  _ <| |___| |  | | |_| |\ V /  | || |\  | |_| |
# |_| \_\_____|_|  |_|\___/  \_/  |___|_| \_|\____|

if args.remove:
    master_pass()
    l = lister()
    alias = args.remove[0]

    if alias not in l:
        while True:
            print(f"Alias '{alias}' does not exist")
            match input("Try other alias? (y/n/l): "):
                case "y" | "Y":
                    alias = input("Input alias: ")
                    if alias in l: break
                case "l" | "L":
                    print("Stored aliases: ")
                    for name in l: print("  ", name)
                case _:
                    if verb != 0: print("Aborting..")
                    sys.exit()

    deleter(name=alias, check=True)

    if verb == 1: print(f"Alias '{alias}' was removed")
    if verb == 2:
        print(f"Alias '{alias}' was removed\nExisting aliases:")
        l = lister()
        for name in l: print("  ", name)
    sys.exit()

#   ____ _   _    _    _   _  ____ ___ _   _  ____
#  / ___| | | |  / \  | \ | |/ ___|_ _| \ | |/ ___|
# | |   | |_| | / _ \ |  \| | |  _ | ||  \| | |  _
# | |___|  _  |/ ___ \| |\  | |_| || || |\  | |_| |
#  \____|_| |_/_/   \_\_| \_|\____|___|_| \_|\____|

if args.change:
    master_pass()
    l = lister()
    alias = args.change[0]

    if alias not in l:
        while True:
            print(f"Alias '{alias}' does not exist")
            match input("Try other alias? (y/n/l): "):
                case "y" | "Y":
                    alias = input("Input alias: ")
                    if alias in l: break
                case "l" | "L":
                    print("Stored aliases: ")
                    for name in l: print("  ", name)
                case _:
                    if verb != 0: print("Aborting..")
                    sys.exit()

    deleter(name=alias, check=False)
    store(encrypt(name=alias))
    if verb > 0: print(f"Password under '{alias}' changed successfully")
    sys.exit()

#   ____ _____ _   _ _____ ____      _  _____ ___ _   _  ____
#  / ___| ____| \ | | ____|  _ \    / \|_   _|_ _| \ | |/ ___|
# | |  _|  _| |  \| |  _| | |_) |  / _ \ | |  | ||  \| | |  _
# | |_| | |___| |\  | |___|  _ <  / ___ \| |  | || |\  | |_| |
#  \____|_____|_| \_|_____|_| \_\/_/   \_\_| |___|_| \_|\____|

if args.generate:
    lvl = args.generate[0]
    length = args.generate[1]

    if lvl not in [0, 1, 2, 3]:
        while True:
            print("Incorrect security level, available levels (0-3)\nType(n/q) to quit")
            match input("Input security level: "):
                case "0" | "1" | "2" | "3" as lvl: break
                case "n" | "N" | "q" | "Q":
                    if verb != 0: print("Aborting..")
                    sys.exit()

    if length not in range(4, 101):
        while True:
            print("Incorrect length, available password length (4-100)\n Type(n/q) to quit")
            length = input("Input length: ")
            try:
                length = int(length)
                if length in range(4, 101):
                    break
            except ValueError: pass

    password = new_password(length=length, level=lvl)
    if verb != 0:
        print("Generated password:", password)
        match input("Want to copy it to the clipboard? (y/n): "):
            case "y" | "Y": pyperclip.copy(password); print("Copied")
    else: pyperclip.copy(password)
    sys.exit()
