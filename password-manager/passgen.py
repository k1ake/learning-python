import random
from globals import lower, upper, digits, extra


def new_password(length, level=2):
    tochoosefrom = list()
    new_password = list()
    match level:
        case 0:
            for i in lower: tochoosefrom.append(i)
            new_password.append(random.choice(lower))
        case 1:
            for i in lower: tochoosefrom.append(i)
            new_password.append(random.choice(lower))
            for i in upper: tochoosefrom.append(i)
            new_password.append(random.choice(upper))
        case 2:
            for i in lower: tochoosefrom.append(i)
            new_password.append(random.choice(lower))
            for i in upper: tochoosefrom.append(i)
            new_password.append(random.choice(upper))
            for i in digits: tochoosefrom.append(i)
            new_password.append(random.choice(digits))
        case 3:
            for i in lower: tochoosefrom.append(i)
            new_password.append(random.choice(lower))
            for i in upper: tochoosefrom.append(i)
            new_password.append(random.choice(upper))
            for i in digits: tochoosefrom.append(i)
            new_password.append(random.choice(digits))
            for i in extra: tochoosefrom.append(i)
            new_password.append(random.choice(extra))
    for i in range(length - level):
        new_password.append(random.choice(tochoosefrom))
    random.shuffle(new_password)
    password = ''.join(new_password)
    return password
