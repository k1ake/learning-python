import os, sys, subprocess, json, argparse
from google_currency import convert as c


def clear(): return os.system("clear")


check = True
# FUNCTIONS


def mass_conv():
    if check == True:
        clear()
        print("Available dimensions: lb, kg \nInput {value} {from} {to}")
        mass = input(": ").split(" ")
        value = float(mass[0])
        mass_f = mass[1].lower()
        mass_t = mass[2].lower()
    else:
        value = float(args.mass[0])
        mass_f = args.mass[1]
        mass_t = args.mass[2]

    match mass_f:
        case "lb": value /= 2.2046
        case "kg": pass
        case _: print("Not known mass1 dimension"); sys.exit()

    match mass_t:
        case "lb": value *= 2.2046
        case "kg": pass
        case _: print("Not known mass2 dimension"); sys.exit()

    print(round(value, 3), mass_t)


def length_conv():
    if check == True:
        clear()
        print("Available dimensions: m, km, ft, yd, mi \nInput {value} {from} {to}")
        length = input(": ").split(" ")
        value = float(length[0])
        len_f = length[1].lower()
        len_t = length[2].lower()
    else:
        value = float(args.length[0])
        len_f = args.length[1]
        len_t = args.length[2]
    match len_f:
        case "m": pass
        case "km": value *= 1000
        case "ft": value *= 0.3048
        case "yd": value *= 0.9144
        case "mi": value *= 1609.344
        case _: print("Not known len1 dimension"); sys.exit()

    match len_t:
        case "m": pass
        case "km": value /= 1000
        case "ft": value *= 3.28084
        case "yd": value *= 1.09361
        case "mi": value *= 0.00062137
        case _: print("Not known len2 dimension"); sys.exit()

    print(round(value, 3), len_t)


def temp_conv():
    if check == True:
        clear()
        print("Available dimensions: c f k \nInput {value} {from} {to}")
        temp = input(": ").split(" ")
        value = float(temp[0])
        temp_f = temp[1].lower()
        temp_t = temp[2].lower()
    else:
        value = float(args.temp[0])
        temp_f = args.temp[1]
        temp_t = args.temp[2]
    match temp_f:
        case "c": pass
        case "f": value = (value - 32) / 1.8
        case "k": value -= 273.15
        case _: print("Not known temp1 dimension"); sys.exit()

    match temp_t:
        case "c": pass
        case "f": value = (value * 1.8) + 32
        case "k": value += 273.15
        case _: print("Not known temp2 dimension"); sys.exit()

    print(round(value, 2), temp_t)


def cur_conv():
    if check == True:
        clear()
        print("Currency name should be written in the form of an abbreviation \n Input {value} {from} {to}")
        currency = input(": ").split(" ")
        value = float(currency[0])
        cur_f = currency[1].lower()
        cur_t = currency[2].lower()
    else:
        value = float(args.currency[0])
        cur_f = args.currency[1]
        cur_t = args.currency[2]
    print(json.loads(c(cur_f, cur_t, value))["amount"], cur_t.upper())

# arguments


parser = argparse.ArgumentParser(description="Just Converts")
convert = parser.add_mutually_exclusive_group()
convert.add_argument("-m", "--mass", nargs=3, metavar=("Value", "Mass1", "Mass2"), help="Converts mass")
convert.add_argument("-l", "--length", nargs=3, metavar=("Value", "Length1", "Length2"), help="Converts length")
convert.add_argument("-t", "--temp", nargs=3, metavar=("Value", "Temp1", "Temp2"), help="Converts temperature")
convert.add_argument("-c", "--currency", nargs=3, metavar=("Value", "Cur1", "Cur2"), help="Converts currency")

args = parser.parse_args()

if len(sys.argv) < 2:
    print("""What do you want to convert?
(1) Masses          (2) Lengths
(3) Temperatures    (4) Currencies""")
    match input("Choose: "):
        case "1": mass_conv()
        case "2": length_conv()
        case "3": temp_conv()
        case "4": cur_conv()
        case _: clear; print("No.")
    sys.exit()


# print(args.mass)

if args.mass is not None:
    check = False
    mass_conv()
elif args.length is not None:
    check = False
    length_conv()
elif args.temp is not None:
    check = False
    temp_conv()
elif args.currency is not None:
    check = False
    cur_conv()
